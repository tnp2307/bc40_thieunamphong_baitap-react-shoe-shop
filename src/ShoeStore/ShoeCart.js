import React, { Component } from "react";

export default class ShoeCart extends Component {
  renderTbody = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.quantitybuy}</td>
          <td>
            <img src={item.image} style={{ width: 50 }} alt="" />
          </td>
          <td>
            <button
              className="btn btn-danger"
              onClick={() => this.props.handleXoa(item.id)}
            >
              -
            </button>
            {item.quantitybuy}
            <button
              className="btn btn-success"
              onClick={() => this.props.handleThem(item.id)}
            >
              +
            </button>
          </td>
          <td>
            <button
              className="btn btn-danger"
              onClick={() => this.props.handleXoaAll(item.id)}
            >
              Xóa
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <th>ID</th>
            <th>Name</th>
            <th>Price</th>
            <th>Img</th>
            <th>Qty</th>
            <th>Thao tác</th>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}
