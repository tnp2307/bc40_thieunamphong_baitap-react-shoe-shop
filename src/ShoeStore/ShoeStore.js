import React, { Component } from "react";
import { dataShoe } from "./DataShoe";
import ShoeCart from "./ShoeCart";
import ShoeDetail from "./ShoeDetail";
import ShoeList from "./ShoeList";

export default class ShoeStore extends Component {
  state = {
    shoe: dataShoe,
    cart: [],
    detail: {},
  };
  handleAddCart = (shoe) => {
    
    let vitri = this.state.cart.findIndex((item) => item.id == shoe.id);
    if (vitri == -1) {
      let cloneCart = [...this.state.cart];
      let newshoe = { ...shoe, quantitybuy: 1 };
      cloneCart.push(newshoe);
      this.setState({
        cart: cloneCart,
      });
    }
    else{
      this.state.cart[vitri].quantitybuy++
      let cloneCart = [...this.state.cart];

      this.setState({
        cart: cloneCart,
      });
    }
  };
  handleThem = (shoeId) => {
    let viTri = this.state.cart.findIndex((item) => item.id == shoeId);
    this.state.cart[viTri].quantitybuy++;
    let newCart = [...this.state.cart];
    this.setState({
      cart: newCart,
    });
  };
  handleXoa = (shoeId) => { let viTri = this.state.cart.findIndex((item) => item.id == shoeId);
    let newCart = [...this.state.cart];
    console.log(viTri);
    if (newCart[viTri].quantitybuy > 1) {
      newCart[viTri].quantitybuy--;
    } else {
      newCart.splice(viTri, 1);
    }
    console.log(newCart);
    this.setState({
      cart: newCart,
    });};
  handleXoaAll = (shoeId) => {
    let viTri = this.state.cart.findIndex((item) => item.id == shoeId);
    let newCart = [...this.state.cart];
    newCart.splice(viTri, 1);
    this.setState({
      cart: newCart,
    });
  };
  handleSetStateModal =(shoe)=>{
    let newDetail ={...shoe}
    
    this.setState({
      detail: newDetail,
    });
    console.log(this.state.detail.shoe);
  }
  render() {
    return (
      <div className="container">
        <h1 className="header bg-warning text-center ">Shoe Cart</h1>
        <ShoeCart cart={this.state.cart}  handleXoa={this.handleXoa}
          handleThem={this.handleThem}
          handleXoaAll={this.handleXoaAll}
          />
        <h1 className="header bg-primary text-center mt-5">Shoe Store</h1>
        <ShoeList
          handleAddCart={this.handleAddCart}
          phone={this.state.shoe}
          handleSetStateModal={this.handleSetStateModal}
        />
        <ShoeDetail detail={this.state.detail} 
        handleAddCart={this.handleAddCart}/>
      </div>
    );
  }
}
