import React, { Component } from "react";
import ShoeItem from "./ShoeItem";

export default class ShoeList extends Component {
  renderShoeList = () => {
    return this.props.phone.map((item) => {
      return (
        <ShoeItem
          handle0nclick={this.props.handleAddCart}
          item={item}
          handleXoaAll={this.props.handleXoaAll}
          handleThem={this.props.handleThem}
          handleXoa={this.props.handleXoa}
          handleSetStateModal={this.props.handleSetStateModal}
        />
      );
    });
  };
  render() {
    return <div className="row ">{this.renderShoeList()}</div>;
  }
}
