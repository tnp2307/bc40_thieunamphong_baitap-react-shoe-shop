import React, { Component } from 'react'

export default class ShoeDetail extends Component {
  render() {
    return (
      <div>
          <div>
        <div
          className="modal fade"
          id="exampleModalCenter"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="exampleModalCenterTitle"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLongTitle">
                  {this.props.detail.name}
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <img
                  className="card-img-top"
                  src={this.props.detail.image}
                  alt="Card image cap"
                />
                {this.props.detail.description}
              </div>

              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  {this.props.detail.price}
                </button>
                <button
                  type="button"
                  className="btn btn-primary"
                  onClick={() => this.props.handleAddCart(this.props.detail)}
                >
                  Add to cart{" "}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
    )
  }
}
