import React, { Component } from "react";

export default class ShoeItem extends Component {
  render() {
    let { image, name, price } = this.props.item;
    return (
      <div className="col-3 py-4">
        <div className="d-flex flex-column">
          <button 
            type="button"
            onClick={() => this.props.handleSetStateModal(this.props.item)}
            data-toggle="modal"
            data-target="#exampleModalCenter"
          >
            <div className="card">
              <div className="card-body">
                <h5 className="card-title">{name}</h5>
                <p className="card-text">{price}</p>
                <img className="card-img-top" src={image} alt="Card image cap" />
              </div>
            </div>
          </button>
          <button
            className="btn btn-primary "
            onClick={() => this.props.handle0nclick(this.props.item)}
          >
            Add to cart
          </button>
        </div>
      </div>
    );
  }
}
