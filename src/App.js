import logo from './logo.svg';
import './App.css';
import ShoeStore from './ShoeStore/ShoeStore';

function App() {
  return (
    <div >
      <ShoeStore/>
    </div>
  );
}

export default App;
